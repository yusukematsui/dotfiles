"--- vundle (begin) ---
set nocompatible
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-unimpaired'

Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

set rtp+=/usr/local/opt/fzf
Plugin 'junegunn/fzf.vim'

call vundle#end()
filetype plugin indent on
" --- vundle (end) ---

" AirlineTheme
let g:airline_theme='light'

"----------------------------------------------------


" Encoding
set encoding=utf-8
"set scriptencoding=utf-8
set fileencoding=utf-8
set fileencodings=iso-2022-jp,sjis,euc-jp,utf-8

" set clipboard=unnamedplus
set statusline=%f\ %{'['.(&fenc!=''?&fenc:'?').'-'.&ff.']'}
set cursorline

" Indentation
" filetype plugin indent on
set shiftwidth=4
set tabstop=4
set expandtab
set autoindent
set smartindent

" Search
set smartcase
set hlsearch
nnoremap <silent><Esc><Esc> :<C-u>set nohlsearch!<CR>

set wildmenu
set history=100

syntax on

" fzf & ripgrep
" command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)

